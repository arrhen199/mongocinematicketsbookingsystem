﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.MongoModels
{
    public static class Settings
    {
        public static string ConnectionString { get; } = "mongodb://admin:abc123!@localhost";
        public static string Database { get; } = "CinemaTicketBookingServiceDb";
    }
}
