﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.MongoModels
{
    public class Movie
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public DateTime EmissionTime { get; set; }
        public ICollection<Seat> Seats { get; set; } = new List<Seat>();

    }
}
