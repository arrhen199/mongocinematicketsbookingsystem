﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.MongoModels
{
    public class Seat
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public bool Booked { get; set; } = false;
        public int SeatNumber { get; set; }
    }
}
