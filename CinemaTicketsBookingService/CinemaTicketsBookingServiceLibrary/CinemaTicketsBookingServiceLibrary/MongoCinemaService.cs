﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.MongoModels;
using System.ServiceModel;
using LogicLayer.MongoLogicLayer;
using MongoDB.Bson;

namespace CinemaTicketsBookingServiceLibrary
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class MongoCinemaService : IMongoCinemaService
    {

        private IMovieRepository movieLogic;
        private IUserRepository userLogic;
        private IReservationRepository reservationLogic;

        public MongoCinemaService()
        {
            movieLogic = new MovieRepository();
            userLogic = new UserRepository();
            reservationLogic = new ReservationRepository();
        }

        public Movie AddMovie(Movie movie)
        {
            movieLogic.AddMovie(movie);
            return movie;
        }

        public User AddUser(User user)
        {
            userLogic.AddUser(user);
            return user;
        }

        public Reservation CreateReservation(Reservation reservation, ObjectId userId)
        {
            if (reservationLogic.CreateReservation(reservation, userId).IsAcknowledged)
            {
                movieLogic.UpdateMovie(reservation.Movie, reservation.SeatNumber, true);
                return reservation;
            }

            return null;
        }

        public bool DeleteReservation(Reservation reservation, ObjectId userId)
        {
            if (reservationLogic.DeleteReservation(reservation, userId).IsAcknowledged)
            {
                movieLogic.UpdateMovie(reservation.Movie, reservation.SeatNumber, false);
                return true;
            }

            return false;
        }

        public List<Movie> GetMovies()
        {
            return movieLogic.GetMovies().ToList();
        }

        public User Login(string login, string password)
        {
            return userLogic.Login(login, password);
        }

        public Movie UpdateMovie(Movie movie, int seat, bool booked)
        {
            movieLogic.UpdateMovie(movie, seat, booked);
            return movie;
        }

        public User UpdateUser(User user)
        {
            userLogic.UpdateUser(user);
            return user;
        }
    }

}
