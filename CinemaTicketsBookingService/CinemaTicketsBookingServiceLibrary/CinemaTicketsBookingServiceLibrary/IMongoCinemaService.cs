﻿using DataModel.MongoModels;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CinemaTicketsBookingServiceLibrary
{
    [ServiceContract]
    public interface IMongoCinemaService
    {
        [OperationContract]
        List<Movie> GetMovies();

        [OperationContract]
        Movie AddMovie(Movie movie);
        [OperationContract]
        Movie UpdateMovie(Movie movie, int seat, bool booked);

        [OperationContract]
        bool DeleteReservation(Reservation reservation, ObjectId userId);
        [OperationContract]
        Reservation CreateReservation(Reservation reservation, ObjectId userId);

        [OperationContract]
        User Login(string login, string password);

        [OperationContract]
        User UpdateUser(User user);

        [OperationContract]
        User AddUser(User user);

    }
}
