﻿using DataModel.MongoModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.MongoDataLayer
{
    public class MovieContext
    {
        private readonly IMongoDatabase _database = null;

        public MovieContext()
        {
            var client = new MongoClient(DataModel.MongoModels.Settings.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(DataModel.MongoModels.Settings.Database);
        }

        public IMongoCollection<Movie> Movies
        {
            get
            {
                return _database.GetCollection<Movie>("Movie");
            }
        }
    }
}
