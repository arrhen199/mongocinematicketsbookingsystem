﻿using DataModel.MongoModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.MongoDataLayer
{
    public class UserContext
    {
        private readonly IMongoDatabase _database = null;

        public UserContext()
        {
            var client = new MongoClient(DataModel.MongoModels.Settings.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(DataModel.MongoModels.Settings.Database);
        }

        public IMongoCollection<User> Users
        {
            get
            {
                return _database.GetCollection<User>("User");
            }
        }
    }
}
