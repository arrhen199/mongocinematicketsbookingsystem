﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.MongoModels;
using MongoDB.Driver;
using DataLayer.MongoDataLayer;

namespace LogicLayer.MongoLogicLayer
{
    public class UserRepository : IUserRepository
    {
        private UserContext context;
        public User AddUser(User user)
        {
            context = new UserContext();

            context.Users.InsertOne(user);
            return user;
        }

        public User Login(string login, string password)
        {
            context = new UserContext();

            var filter = (Builders<User>.Filter.Eq(s => s.Login, login)) & (Builders<User>.Filter.Eq(s => s.Password, password));
            return context.Users.Find(filter).FirstOrDefault();
        }

        public ReplaceOneResult UpdateUser(User user)
        {
            context = new UserContext();

            return context.Users
                             .ReplaceOne(n => n.Id.Equals(user.Id)
                                                 , user
                                                 , new UpdateOptions { IsUpsert = true });
        }

    }
}
