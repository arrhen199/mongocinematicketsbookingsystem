﻿using DataModel.MongoModels;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.MongoLogicLayer
{
    public interface IReservationRepository
    {
        UpdateResult CreateReservation(Reservation reservation, ObjectId userId);
        UpdateResult DeleteReservation(Reservation reservation, ObjectId userId); 
    }
}
