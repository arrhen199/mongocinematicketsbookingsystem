﻿using DataModel.MongoModels;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.MongoLogicLayer
{
    public interface IMovieRepository
    {
        ICollection<Movie> GetMovies();
        void AddMovie(Movie movie);
        Movie GetMovie(ObjectId id);
        void UpdateMovie(Movie movie, int seat, bool booked);

    }
}
