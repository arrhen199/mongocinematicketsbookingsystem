﻿using DataModel.MongoModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.MongoLogicLayer
{
    public interface IUserRepository
    {
        User Login(string login, string password);
        User AddUser(User user);
        ReplaceOneResult UpdateUser(User user);
    }
}
