﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.MongoModels;
using MongoDB.Bson;
using MongoDB.Driver;
using DataLayer.MongoDataLayer;

namespace LogicLayer.MongoLogicLayer
{
    public class ReservationRepository : IReservationRepository
    {
        private UserContext context;
        public UpdateResult CreateReservation(Reservation reservation, ObjectId userId)
        {
            context = new UserContext();
            reservation.Id = ObjectId.GenerateNewId();
            return context.Users.UpdateOne(u => u.Id == userId, Builders<User>.
                Update.Push<Reservation>("Reservations", reservation));
            
        }

        public UpdateResult DeleteReservation(Reservation reservation, ObjectId userId)
        {
            context = new UserContext();

            return context.Users.UpdateOne(u => u.Id == userId, Builders<User>.
                Update.Pull<Reservation>("Reservations", reservation));
        }
    }
}
