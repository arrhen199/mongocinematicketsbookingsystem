﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.MongoModels;
using DataLayer.MongoDataLayer;
using MongoDB.Driver;
using MongoDB.Bson;

namespace LogicLayer.MongoLogicLayer
{
    public class MovieRepository : IMovieRepository
    {
        private MovieContext context;
        public void AddMovie(Movie movie)
        {
            context = new MovieContext();

            foreach (var seat in movie.Seats)
            {
                seat.Id = ObjectId.GenerateNewId();
            }

            context.Movies.InsertOne(movie);
            
        }

        public Movie GetMovie(ObjectId id)
        {
            context = new MovieContext();

            return context.Movies.Find(x=>x.Id == id).FirstOrDefault();
        }

        public ICollection<Movie> GetMovies()
        {
            context = new MovieContext();

            return context.Movies.Find(_ => true).ToList();
        }

        public void UpdateMovie(Movie movie, int seat, bool booked)
        {
            context = new MovieContext();

            var movieToUpdate = context.Movies.Find(x => x.Id == movie.Id).First();
            var dbSeat = movieToUpdate.Seats.Where(x => x.SeatNumber == seat).FirstOrDefault();

            dbSeat.Booked = booked;

            context.Movies.ReplaceOne(x => x.Id == movieToUpdate.Id, movieToUpdate);
                      
            //context.Movies.UpdateOne(u => u.Id == movie.Id, Builders<Movie>.
            //    Update.Set<Seat>(x=>x.Seats.Where(c=>c.Id == dbSeat.Id).FirstOrDefault(), dbSeat));


        }
    }
}
