﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CinemaTicketsBookingClient.MongoCinemaService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Movie", Namespace="http://schemas.datacontract.org/2004/07/DataModel.MongoModels")]
    [System.SerializableAttribute()]
    public partial class Movie : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime EmissionTimeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.MongoCinemaService.ObjectId IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.MongoCinemaService.Seat[] SeatsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TitleField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime EmissionTime {
            get {
                return this.EmissionTimeField;
            }
            set {
                if ((this.EmissionTimeField.Equals(value) != true)) {
                    this.EmissionTimeField = value;
                    this.RaisePropertyChanged("EmissionTime");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.MongoCinemaService.ObjectId Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.MongoCinemaService.Seat[] Seats {
            get {
                return this.SeatsField;
            }
            set {
                if ((object.ReferenceEquals(this.SeatsField, value) != true)) {
                    this.SeatsField = value;
                    this.RaisePropertyChanged("Seats");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Title {
            get {
                return this.TitleField;
            }
            set {
                if ((object.ReferenceEquals(this.TitleField, value) != true)) {
                    this.TitleField = value;
                    this.RaisePropertyChanged("Title");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ObjectId", Namespace="http://schemas.datacontract.org/2004/07/MongoDB.Bson")]
    [System.SerializableAttribute()]
    public partial struct ObjectId : System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private int _aField;
        
        private int _bField;
        
        private int _cField;
        
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _a {
            get {
                return this._aField;
            }
            set {
                if ((this._aField.Equals(value) != true)) {
                    this._aField = value;
                    this.RaisePropertyChanged("_a");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _b {
            get {
                return this._bField;
            }
            set {
                if ((this._bField.Equals(value) != true)) {
                    this._bField = value;
                    this.RaisePropertyChanged("_b");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _c {
            get {
                return this._cField;
            }
            set {
                if ((this._cField.Equals(value) != true)) {
                    this._cField = value;
                    this.RaisePropertyChanged("_c");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Seat", Namespace="http://schemas.datacontract.org/2004/07/DataModel.MongoModels")]
    [System.SerializableAttribute()]
    public partial class Seat : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool BookedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.MongoCinemaService.ObjectId IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int SeatNumberField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Booked {
            get {
                return this.BookedField;
            }
            set {
                if ((this.BookedField.Equals(value) != true)) {
                    this.BookedField = value;
                    this.RaisePropertyChanged("Booked");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.MongoCinemaService.ObjectId Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SeatNumber {
            get {
                return this.SeatNumberField;
            }
            set {
                if ((this.SeatNumberField.Equals(value) != true)) {
                    this.SeatNumberField = value;
                    this.RaisePropertyChanged("SeatNumber");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Reservation", Namespace="http://schemas.datacontract.org/2004/07/DataModel.MongoModels")]
    [System.SerializableAttribute()]
    public partial class Reservation : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.MongoCinemaService.ObjectId IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.MongoCinemaService.Movie MovieField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int SeatNumberField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.MongoCinemaService.ObjectId Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.MongoCinemaService.Movie Movie {
            get {
                return this.MovieField;
            }
            set {
                if ((object.ReferenceEquals(this.MovieField, value) != true)) {
                    this.MovieField = value;
                    this.RaisePropertyChanged("Movie");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SeatNumber {
            get {
                return this.SeatNumberField;
            }
            set {
                if ((this.SeatNumberField.Equals(value) != true)) {
                    this.SeatNumberField = value;
                    this.RaisePropertyChanged("SeatNumber");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="User", Namespace="http://schemas.datacontract.org/2004/07/DataModel.MongoModels")]
    [System.SerializableAttribute()]
    public partial class User : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.MongoCinemaService.ObjectId IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string LoginField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PasswordField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.MongoCinemaService.Reservation[] ReservationsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.MongoCinemaService.ObjectId Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Login {
            get {
                return this.LoginField;
            }
            set {
                if ((object.ReferenceEquals(this.LoginField, value) != true)) {
                    this.LoginField = value;
                    this.RaisePropertyChanged("Login");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Password {
            get {
                return this.PasswordField;
            }
            set {
                if ((object.ReferenceEquals(this.PasswordField, value) != true)) {
                    this.PasswordField = value;
                    this.RaisePropertyChanged("Password");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.MongoCinemaService.Reservation[] Reservations {
            get {
                return this.ReservationsField;
            }
            set {
                if ((object.ReferenceEquals(this.ReservationsField, value) != true)) {
                    this.ReservationsField = value;
                    this.RaisePropertyChanged("Reservations");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="MongoCinemaService.IMongoCinemaService")]
    public interface IMongoCinemaService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/GetMovies", ReplyAction="http://tempuri.org/IMongoCinemaService/GetMoviesResponse")]
        CinemaTicketsBookingClient.MongoCinemaService.Movie[] GetMovies();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/GetMovies", ReplyAction="http://tempuri.org/IMongoCinemaService/GetMoviesResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.Movie[]> GetMoviesAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/AddMovie", ReplyAction="http://tempuri.org/IMongoCinemaService/AddMovieResponse")]
        CinemaTicketsBookingClient.MongoCinemaService.Movie AddMovie(CinemaTicketsBookingClient.MongoCinemaService.Movie movie);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/AddMovie", ReplyAction="http://tempuri.org/IMongoCinemaService/AddMovieResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.Movie> AddMovieAsync(CinemaTicketsBookingClient.MongoCinemaService.Movie movie);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/UpdateMovie", ReplyAction="http://tempuri.org/IMongoCinemaService/UpdateMovieResponse")]
        CinemaTicketsBookingClient.MongoCinemaService.Movie UpdateMovie(CinemaTicketsBookingClient.MongoCinemaService.Movie movie, int seat, bool booked);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/UpdateMovie", ReplyAction="http://tempuri.org/IMongoCinemaService/UpdateMovieResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.Movie> UpdateMovieAsync(CinemaTicketsBookingClient.MongoCinemaService.Movie movie, int seat, bool booked);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/DeleteReservation", ReplyAction="http://tempuri.org/IMongoCinemaService/DeleteReservationResponse")]
        bool DeleteReservation(CinemaTicketsBookingClient.MongoCinemaService.Reservation reservation, CinemaTicketsBookingClient.MongoCinemaService.ObjectId userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/DeleteReservation", ReplyAction="http://tempuri.org/IMongoCinemaService/DeleteReservationResponse")]
        System.Threading.Tasks.Task<bool> DeleteReservationAsync(CinemaTicketsBookingClient.MongoCinemaService.Reservation reservation, CinemaTicketsBookingClient.MongoCinemaService.ObjectId userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/CreateReservation", ReplyAction="http://tempuri.org/IMongoCinemaService/CreateReservationResponse")]
        CinemaTicketsBookingClient.MongoCinemaService.Reservation CreateReservation(CinemaTicketsBookingClient.MongoCinemaService.Reservation reservation, CinemaTicketsBookingClient.MongoCinemaService.ObjectId userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/CreateReservation", ReplyAction="http://tempuri.org/IMongoCinemaService/CreateReservationResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.Reservation> CreateReservationAsync(CinemaTicketsBookingClient.MongoCinemaService.Reservation reservation, CinemaTicketsBookingClient.MongoCinemaService.ObjectId userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/Login", ReplyAction="http://tempuri.org/IMongoCinemaService/LoginResponse")]
        CinemaTicketsBookingClient.MongoCinemaService.User Login([System.ServiceModel.MessageParameterAttribute(Name="login")] string login1, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/Login", ReplyAction="http://tempuri.org/IMongoCinemaService/LoginResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.User> LoginAsync(string login, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/UpdateUser", ReplyAction="http://tempuri.org/IMongoCinemaService/UpdateUserResponse")]
        CinemaTicketsBookingClient.MongoCinemaService.User UpdateUser(CinemaTicketsBookingClient.MongoCinemaService.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/UpdateUser", ReplyAction="http://tempuri.org/IMongoCinemaService/UpdateUserResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.User> UpdateUserAsync(CinemaTicketsBookingClient.MongoCinemaService.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/AddUser", ReplyAction="http://tempuri.org/IMongoCinemaService/AddUserResponse")]
        CinemaTicketsBookingClient.MongoCinemaService.User AddUser(CinemaTicketsBookingClient.MongoCinemaService.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMongoCinemaService/AddUser", ReplyAction="http://tempuri.org/IMongoCinemaService/AddUserResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.User> AddUserAsync(CinemaTicketsBookingClient.MongoCinemaService.User user);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMongoCinemaServiceChannel : CinemaTicketsBookingClient.MongoCinemaService.IMongoCinemaService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MongoCinemaServiceClient : System.ServiceModel.ClientBase<CinemaTicketsBookingClient.MongoCinemaService.IMongoCinemaService>, CinemaTicketsBookingClient.MongoCinemaService.IMongoCinemaService {
        
        public MongoCinemaServiceClient() {
        }
        
        public MongoCinemaServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MongoCinemaServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MongoCinemaServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MongoCinemaServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public CinemaTicketsBookingClient.MongoCinemaService.Movie[] GetMovies() {
            return base.Channel.GetMovies();
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.Movie[]> GetMoviesAsync() {
            return base.Channel.GetMoviesAsync();
        }
        
        public CinemaTicketsBookingClient.MongoCinemaService.Movie AddMovie(CinemaTicketsBookingClient.MongoCinemaService.Movie movie) {
            return base.Channel.AddMovie(movie);
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.Movie> AddMovieAsync(CinemaTicketsBookingClient.MongoCinemaService.Movie movie) {
            return base.Channel.AddMovieAsync(movie);
        }
        
        public CinemaTicketsBookingClient.MongoCinemaService.Movie UpdateMovie(CinemaTicketsBookingClient.MongoCinemaService.Movie movie, int seat, bool booked) {
            return base.Channel.UpdateMovie(movie, seat, booked);
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.Movie> UpdateMovieAsync(CinemaTicketsBookingClient.MongoCinemaService.Movie movie, int seat, bool booked) {
            return base.Channel.UpdateMovieAsync(movie, seat, booked);
        }
        
        public bool DeleteReservation(CinemaTicketsBookingClient.MongoCinemaService.Reservation reservation, CinemaTicketsBookingClient.MongoCinemaService.ObjectId userId) {
            return base.Channel.DeleteReservation(reservation, userId);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteReservationAsync(CinemaTicketsBookingClient.MongoCinemaService.Reservation reservation, CinemaTicketsBookingClient.MongoCinemaService.ObjectId userId) {
            return base.Channel.DeleteReservationAsync(reservation, userId);
        }
        
        public CinemaTicketsBookingClient.MongoCinemaService.Reservation CreateReservation(CinemaTicketsBookingClient.MongoCinemaService.Reservation reservation, CinemaTicketsBookingClient.MongoCinemaService.ObjectId userId) {
            return base.Channel.CreateReservation(reservation, userId);
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.Reservation> CreateReservationAsync(CinemaTicketsBookingClient.MongoCinemaService.Reservation reservation, CinemaTicketsBookingClient.MongoCinemaService.ObjectId userId) {
            return base.Channel.CreateReservationAsync(reservation, userId);
        }
        
        public CinemaTicketsBookingClient.MongoCinemaService.User Login(string login1, string password) {
            return base.Channel.Login(login1, password);
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.User> LoginAsync(string login, string password) {
            return base.Channel.LoginAsync(login, password);
        }
        
        public CinemaTicketsBookingClient.MongoCinemaService.User UpdateUser(CinemaTicketsBookingClient.MongoCinemaService.User user) {
            return base.Channel.UpdateUser(user);
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.User> UpdateUserAsync(CinemaTicketsBookingClient.MongoCinemaService.User user) {
            return base.Channel.UpdateUserAsync(user);
        }
        
        public CinemaTicketsBookingClient.MongoCinemaService.User AddUser(CinemaTicketsBookingClient.MongoCinemaService.User user) {
            return base.Channel.AddUser(user);
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.MongoCinemaService.User> AddUserAsync(CinemaTicketsBookingClient.MongoCinemaService.User user) {
            return base.Channel.AddUserAsync(user);
        }
    }
}
