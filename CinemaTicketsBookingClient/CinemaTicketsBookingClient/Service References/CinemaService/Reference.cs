﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CinemaTicketsBookingClient.CinemaService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Movie", Namespace="http://schemas.datacontract.org/2004/07/DataModel.Models")]
    [System.SerializableAttribute()]
    public partial class Movie : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime EmissionTimeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Seat> SeatsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TitleField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime EmissionTime {
            get {
                return this.EmissionTimeField;
            }
            set {
                if ((this.EmissionTimeField.Equals(value) != true)) {
                    this.EmissionTimeField = value;
                    this.RaisePropertyChanged("EmissionTime");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Seat> Seats {
            get {
                return this.SeatsField;
            }
            set {
                if ((object.ReferenceEquals(this.SeatsField, value) != true)) {
                    this.SeatsField = value;
                    this.RaisePropertyChanged("Seats");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Title {
            get {
                return this.TitleField;
            }
            set {
                if ((object.ReferenceEquals(this.TitleField, value) != true)) {
                    this.TitleField = value;
                    this.RaisePropertyChanged("Title");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Seat", Namespace="http://schemas.datacontract.org/2004/07/DataModel.Models")]
    [System.SerializableAttribute()]
    public partial class Seat : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool BookedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int SeatNumberField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Booked {
            get {
                return this.BookedField;
            }
            set {
                if ((this.BookedField.Equals(value) != true)) {
                    this.BookedField = value;
                    this.RaisePropertyChanged("Booked");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SeatNumber {
            get {
                return this.SeatNumberField;
            }
            set {
                if ((this.SeatNumberField.Equals(value) != true)) {
                    this.SeatNumberField = value;
                    this.RaisePropertyChanged("SeatNumber");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="User", Namespace="http://schemas.datacontract.org/2004/07/DataModel.Models")]
    [System.SerializableAttribute()]
    public partial class User : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string LoginField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PasswordField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Reservation> ReservationsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Login {
            get {
                return this.LoginField;
            }
            set {
                if ((object.ReferenceEquals(this.LoginField, value) != true)) {
                    this.LoginField = value;
                    this.RaisePropertyChanged("Login");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Password {
            get {
                return this.PasswordField;
            }
            set {
                if ((object.ReferenceEquals(this.PasswordField, value) != true)) {
                    this.PasswordField = value;
                    this.RaisePropertyChanged("Password");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Reservation> Reservations {
            get {
                return this.ReservationsField;
            }
            set {
                if ((object.ReferenceEquals(this.ReservationsField, value) != true)) {
                    this.ReservationsField = value;
                    this.RaisePropertyChanged("Reservations");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Reservation", Namespace="http://schemas.datacontract.org/2004/07/DataModel.Models")]
    [System.SerializableAttribute()]
    public partial class Reservation : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.CinemaService.Movie MovieField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.CinemaService.Seat SeatField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private CinemaTicketsBookingClient.CinemaService.User UserField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.CinemaService.Movie Movie {
            get {
                return this.MovieField;
            }
            set {
                if ((object.ReferenceEquals(this.MovieField, value) != true)) {
                    this.MovieField = value;
                    this.RaisePropertyChanged("Movie");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.CinemaService.Seat Seat {
            get {
                return this.SeatField;
            }
            set {
                if ((object.ReferenceEquals(this.SeatField, value) != true)) {
                    this.SeatField = value;
                    this.RaisePropertyChanged("Seat");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public CinemaTicketsBookingClient.CinemaService.User User {
            get {
                return this.UserField;
            }
            set {
                if ((object.ReferenceEquals(this.UserField, value) != true)) {
                    this.UserField = value;
                    this.RaisePropertyChanged("User");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CinemaService.ICinemaService")]
    public interface ICinemaService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetMovies", ReplyAction="http://tempuri.org/ICinemaService/GetMoviesResponse")]
        System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Movie> GetMovies();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetMovies", ReplyAction="http://tempuri.org/ICinemaService/GetMoviesResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Movie>> GetMoviesAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetReservations", ReplyAction="http://tempuri.org/ICinemaService/GetReservationsResponse")]
        System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Reservation> GetReservations(CinemaTicketsBookingClient.CinemaService.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetReservations", ReplyAction="http://tempuri.org/ICinemaService/GetReservationsResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Reservation>> GetReservationsAsync(CinemaTicketsBookingClient.CinemaService.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/CreateReservation", ReplyAction="http://tempuri.org/ICinemaService/CreateReservationResponse")]
        bool CreateReservation(CinemaTicketsBookingClient.CinemaService.Movie movie, CinemaTicketsBookingClient.CinemaService.User user, CinemaTicketsBookingClient.CinemaService.Seat seat);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/CreateReservation", ReplyAction="http://tempuri.org/ICinemaService/CreateReservationResponse")]
        System.Threading.Tasks.Task<bool> CreateReservationAsync(CinemaTicketsBookingClient.CinemaService.Movie movie, CinemaTicketsBookingClient.CinemaService.User user, CinemaTicketsBookingClient.CinemaService.Seat seat);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/UpdateReservation", ReplyAction="http://tempuri.org/ICinemaService/UpdateReservationResponse")]
        bool UpdateReservation(CinemaTicketsBookingClient.CinemaService.Reservation reservation);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/UpdateReservation", ReplyAction="http://tempuri.org/ICinemaService/UpdateReservationResponse")]
        System.Threading.Tasks.Task<bool> UpdateReservationAsync(CinemaTicketsBookingClient.CinemaService.Reservation reservation);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/DeleteReservation", ReplyAction="http://tempuri.org/ICinemaService/DeleteReservationResponse")]
        bool DeleteReservation(CinemaTicketsBookingClient.CinemaService.Reservation reservation);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/DeleteReservation", ReplyAction="http://tempuri.org/ICinemaService/DeleteReservationResponse")]
        System.Threading.Tasks.Task<bool> DeleteReservationAsync(CinemaTicketsBookingClient.CinemaService.Reservation reservation);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/Login", ReplyAction="http://tempuri.org/ICinemaService/LoginResponse")]
        CinemaTicketsBookingClient.CinemaService.User Login(CinemaTicketsBookingClient.CinemaService.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/Login", ReplyAction="http://tempuri.org/ICinemaService/LoginResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.CinemaService.User> LoginAsync(CinemaTicketsBookingClient.CinemaService.User user);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/SetSeatBooked", ReplyAction="http://tempuri.org/ICinemaService/SetSeatBookedResponse")]
        CinemaTicketsBookingClient.CinemaService.Seat SetSeatBooked(CinemaTicketsBookingClient.CinemaService.Seat seat);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/SetSeatBooked", ReplyAction="http://tempuri.org/ICinemaService/SetSeatBookedResponse")]
        System.Threading.Tasks.Task<CinemaTicketsBookingClient.CinemaService.Seat> SetSeatBookedAsync(CinemaTicketsBookingClient.CinemaService.Seat seat);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICinemaServiceChannel : CinemaTicketsBookingClient.CinemaService.ICinemaService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CinemaServiceClient : System.ServiceModel.ClientBase<CinemaTicketsBookingClient.CinemaService.ICinemaService>, CinemaTicketsBookingClient.CinemaService.ICinemaService {
        
        public CinemaServiceClient() {
        }
        
        public CinemaServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CinemaServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CinemaServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CinemaServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Movie> GetMovies() {
            return base.Channel.GetMovies();
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Movie>> GetMoviesAsync() {
            return base.Channel.GetMoviesAsync();
        }
        
        public System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Reservation> GetReservations(CinemaTicketsBookingClient.CinemaService.User user) {
            return base.Channel.GetReservations(user);
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<CinemaTicketsBookingClient.CinemaService.Reservation>> GetReservationsAsync(CinemaTicketsBookingClient.CinemaService.User user) {
            return base.Channel.GetReservationsAsync(user);
        }
        
        public bool CreateReservation(CinemaTicketsBookingClient.CinemaService.Movie movie, CinemaTicketsBookingClient.CinemaService.User user, CinemaTicketsBookingClient.CinemaService.Seat seat) {
            return base.Channel.CreateReservation(movie, user, seat);
        }
        
        public System.Threading.Tasks.Task<bool> CreateReservationAsync(CinemaTicketsBookingClient.CinemaService.Movie movie, CinemaTicketsBookingClient.CinemaService.User user, CinemaTicketsBookingClient.CinemaService.Seat seat) {
            return base.Channel.CreateReservationAsync(movie, user, seat);
        }
        
        public bool UpdateReservation(CinemaTicketsBookingClient.CinemaService.Reservation reservation) {
            return base.Channel.UpdateReservation(reservation);
        }
        
        public System.Threading.Tasks.Task<bool> UpdateReservationAsync(CinemaTicketsBookingClient.CinemaService.Reservation reservation) {
            return base.Channel.UpdateReservationAsync(reservation);
        }
        
        public bool DeleteReservation(CinemaTicketsBookingClient.CinemaService.Reservation reservation) {
            return base.Channel.DeleteReservation(reservation);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteReservationAsync(CinemaTicketsBookingClient.CinemaService.Reservation reservation) {
            return base.Channel.DeleteReservationAsync(reservation);
        }
        
        public CinemaTicketsBookingClient.CinemaService.User Login(CinemaTicketsBookingClient.CinemaService.User user) {
            return base.Channel.Login(user);
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.CinemaService.User> LoginAsync(CinemaTicketsBookingClient.CinemaService.User user) {
            return base.Channel.LoginAsync(user);
        }
        
        public CinemaTicketsBookingClient.CinemaService.Seat SetSeatBooked(CinemaTicketsBookingClient.CinemaService.Seat seat) {
            return base.Channel.SetSeatBooked(seat);
        }
        
        public System.Threading.Tasks.Task<CinemaTicketsBookingClient.CinemaService.Seat> SetSeatBookedAsync(CinemaTicketsBookingClient.CinemaService.Seat seat) {
            return base.Channel.SetSeatBookedAsync(seat);
        }
    }
}
