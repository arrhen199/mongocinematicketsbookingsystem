﻿using CinemaTicketsBookingClient.MongoCinemaService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaTicketsBookingClient
{
    class Program
    {
        public MongoCinemaServiceClient mongoserviceClient;
        public User user;
        public List<Movie> movies;
        List<Reservation> usersReservations;

        public Program()
        {
            mongoserviceClient = new MongoCinemaServiceClient("BasicHttpBinding_IMongoCinemaService");
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            //program.initDb();
            do
            {
                string input = Console.ReadLine();
                string[] words = input.Split(' ');

                switch (words[0])
                {
                    case "reservations":
                        Console.WriteLine(program.GetReservations());
                        break;
                    case "updateReservation":
                        try
                        {
                            Console.WriteLine(program.UpdateReservation(Int32.Parse(words[1]), Int32.Parse(words[2]), Int32.Parse(words[3])));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(availableCommands());
                        }
                        break;
                    case "movies":
                        Console.WriteLine(program.GetMovies());
                        break;
                    case "login":
                        try
                        {
                            Console.WriteLine(program.Login(words[1], words[2]));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(availableCommands());
                        }
                        break;
                    case "user":
                        if (program.user != null)
                            Console.WriteLine(program.user.Login);
                        else
                            Console.WriteLine("Nobody is logged in");
                        break;
                    case "deleteReservation":
                        try
                        {
                            Console.WriteLine(program.deleteReservation(Int32.Parse(words[1])));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(availableCommands());
                        }
                        break;
                    case "reserve":
                        try
                        {
                            Console.WriteLine(program.Reserve(Int32.Parse(words[1]), Int32.Parse(words[2])));
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(availableCommands());
                        }
                        break;
                    case "exit":
                        program.mongoserviceClient.Close();
                        Environment.Exit(0);
                        break;
                    case "help":
                        Console.WriteLine(availableCommands());
                        break;
                    case "logout":
                        program.Logout();
                        break;
                    default:
                        Console.WriteLine(availableCommands());
                        break;
                }

            } while (true);
        }

        public static string availableCommands()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Available commands : ");
            sb.AppendLine(" - 'login yourLogin yourPassword'                               -  To login ");
            sb.AppendLine(" - 'movies'                                                     -  To check avaiable movies  ");
            sb.AppendLine(" - 'reserve movieNumber seatNumber'                             -  To book a seat  ");
            sb.AppendLine(" - 'reservations'                                               -  To check your reservations  ");
            sb.AppendLine(" - 'user'                                                       -  To check logged user login  ");
            sb.AppendLine(" - 'deleteReservation reservationNumber'                        -  To delete reservation   ");
            sb.AppendLine(" - 'logout'                                                     -  To logout  ");
            sb.AppendLine(" - 'exit'                                                       -  To exit program  ");
            return sb.ToString();
        }
        public string GetMovies()
        {

            try
            {
                movies = mongoserviceClient.GetMovies().ToList();
            }
            catch (Exception)
            {

                return "Problem with connection to the service";
            }


            StringBuilder sb = new StringBuilder();
            int i = 1;
            foreach (Movie movie in movies)
            {
                sb.AppendLine();
                sb.AppendLine(String.Format("{0} \"{1}\" {2} {3} ", i, movie.Title, movie.EmissionTime.Date.ToShortDateString(), movie.EmissionTime.ToShortTimeString()));
                sb.AppendLine("Free seats :");
                sb.AppendLine("");
                foreach (Seat seat in movie.Seats)
                {
                    if (seat.Booked == false)
                        sb.Append(String.Format(" {0} ", seat.SeatNumber));
                }
                sb.AppendLine();
                i++;
            }
            sb.AppendLine();
            return sb.ToString();
        }
        public string Login(string login, string password)
        {
            User tmpUser = new User { Login = login, Password = password };
            try
            {
                user = mongoserviceClient.Login(login, password);
            }
            catch (Exception)
            {

                return "Problem with connection to the service";
            }

            if (user == null)
                return "Login or password doesnt match";
            else
                return "hello " + user.Name;
        }
        public string Reserve(int movieNumber, int seatNumber)
        {
            if (user == null)
                return "You must log in !";

            try
            {
                movies = mongoserviceClient.GetMovies().ToList();
            }
            catch (Exception)
            {

                return "Problem with connection to the service";
            }


            Movie movie;
            try
            {
                movie = movies.ElementAt(movieNumber - 1);
            }
            catch (Exception)
            {
                return "Wrong movie number !";
            }
            if (movie == null)
                return "Wrong title !";
            var seat = movie.Seats.Where(x => x.SeatNumber == seatNumber && x.Booked == false).FirstOrDefault();
            if (seat == null)
                return "Wrong seat !";
            
            var userReservations = user.Reservations.ToList();


            Reservation reservation = new Reservation();
            reservation.Movie = movie;
            reservation.SeatNumber = seat.SeatNumber;

            var result = mongoserviceClient.CreateReservation(reservation, user.Id);

            if (result != null)
            {
                userReservations.Add(result);
                user.Reservations = userReservations.ToArray();

                return "Reservation successful";
            }
            else
            {
                return "Something gone wrong";
            }
        }
        public string GetReservations()
        {
            if (user == null)
                return "You must log in !";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" Reservations for  " + user.Name);
            int i = 1;
            if (user.Reservations == null)
                return "there are no reservations for :" + user.Name;
            foreach (Reservation reserv in user.Reservations)
            {
                sb.AppendLine(String.Format(" {0} \"{1}\" {2} {3} {4} ", i, reserv.Movie.Title, reserv.Movie.EmissionTime.Date.ToShortDateString(), reserv.Movie.EmissionTime.ToShortTimeString(), reserv.SeatNumber));
                i++;
            }
            return sb.ToString();

        }
        public void Logout()
        {
            user = null;
            usersReservations = null;
        }
        public string UpdateReservation(int reservNumber, int movieNumber, int seatNumber)
        {
            if (user == null)
                return "You must log in !";
            //movies = serviceClient.GetMovies();
            //usersReservations = serviceClient.GetReservations(user);
            Movie movie;
            Reservation res;
            try
            {
                movie = movies.ElementAt(movieNumber - 1);
                res = usersReservations.ElementAt(reservNumber - 1);
            }
            catch (Exception)
            {
                return "Wrong movie or reservation number !";
            }
            var seat = movie.Seats.Where(x => x.SeatNumber == seatNumber && x.Booked == false).FirstOrDefault();
            if (seat == null)
                return "Wrong seat !";
            res.Movie = movie;
            //res.Seat = seat;
            //if (serviceClient.UpdateReservation(res))
            //    return "Reservations updated succesfully";
            //else
            return "Something gone wrong";
        }
        public string deleteReservation(int reservNumber)
        {
            if (user == null)
                return "You must log in !";
            usersReservations = user.Reservations.ToList();
            Reservation res;
            try
            {
                res = usersReservations.ElementAt(reservNumber - 1);
            }
            catch (Exception)
            {
                return "Wrong reservation number !";
            }
            if (mongoserviceClient.DeleteReservation(res, user.Id))
            {
                usersReservations.Remove(res);
                user.Reservations = usersReservations.ToArray();
                return "Reservations deleted succesfully";
            }
            else
                return "Something gone wrong";
        }
        public void initDb()
        {
            Movie movie1 = new Movie { Title = "Logan", EmissionTime = new DateTime(2017, 7, 21, 20, 45, 00) };
            Movie movie2 = new Movie { Title = "Kong : Skull Island", EmissionTime = new DateTime(2017, 7, 22, 15, 45, 00) };


            movie1.Seats = new Seat[20];
            movie2.Seats = new Seat[20];

            for (int i = 0; i < 20; i++)
            {
                movie1.Seats[i] = new Seat { Booked = false, SeatNumber = i + 1 };
                movie2.Seats[i] = new Seat { Booked = false, SeatNumber = i + 1 };
            }

            mongoserviceClient.AddMovie(movie1);
            mongoserviceClient.AddMovie(movie2);

            User user = new User { Login = "rsi", Name = "Alina", Password = "rsi", Reservations = new Reservation[0] };
            mongoserviceClient.AddUser(user);

        }
    }
}
